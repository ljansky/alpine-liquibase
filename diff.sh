#!/bin/bash
liquibase \
  --changeLogFile=/tmp/changelog.xml \
  --url="jdbc:${DB_JDBC_TYPE}://${DB_TEMP_HOST}/${DB_NAME}" \
  --username=${DB_USER_NAME} \
  --password=${DB_PASSWORD} \
  update

filename=`date +"%Y-%m-%d_%H-%M-%S"`

liquibase \
  --url="jdbc:${DB_JDBC_TYPE}://${DB_TEMP_HOST}/${DB_NAME}" \
  --username=${DB_USER_NAME} \
  --password=${DB_PASSWORD} \
  --logLevel=off \
  diffChangeLog \
  --referenceUrl="jdbc:${DB_JDBC_TYPE}://${DB_HOST}/${DB_NAME}" \
  --referenceUsername=${DB_USER_NAME} \
  --referencePassword=${DB_PASSWORD} \
  | sed -n '1!p' \
  | sed 's/TIMESTAMP(19)/TIMESTAMP/g' \
  | sed 's/type=\"BIT\"/type=\"TINYINT(1)\"/g' > /tmp/changelogs/$(echo $filename).xml