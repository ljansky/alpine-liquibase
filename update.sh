#!/bin/bash
liquibase \
  --changeLogFile=/tmp/changelog.xml \
  --url="jdbc:${DB_JDBC_TYPE}://${DB_HOST}/${DB_NAME}" \
  --username=${DB_USER_NAME} \
  --password=${DB_PASSWORD} \
  update