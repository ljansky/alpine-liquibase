#!/bin/bash

if [ $1 == "update" ] ; then
	/usr/local/bin/update.sh
fi

if [ $1 == "diff" ] ; then
	/usr/local/bin/diff.sh
fi